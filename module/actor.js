import {
  DiceRollerDialogue
} from "./dialogue-diceRoller.js";
import {
  ImprovisedSpellDialogue
} from "./dialogue-improvisedSpell.js";
/**
 * Override and extend the basic :class:`Actor` implementation
 */
export class ActorMtA extends Actor {

  /* -------------------------------------------- */
  /*	Data Preparation														*/
  /* -------------------------------------------- */

  /**
   * Augment the basic Actor data model with additional dynamic data.
   */
  prepareData() {
    super.prepareData();
    

    // Get the Actor's data object
    const actorData = this.data;
    const data = actorData.data;

    if(!data.derivedTraits) derivedTraits = {
      size: {value: 0, mod: 0},
      speed: {value: 0, mod: 0},
      defense: {value: 0, mod: 0},
      armor: {value: 0, mod: 0},
      ballistic: {value: 0, mod: 0},
      initiativeMod: {value: 0, mod: 0},
      perception: {value: 0, mod: 0}
    };

    if (actorData.type === "character") {

      //Get modifiers from items
      let item_mods = actorData.items.reduce((acc, item) => {
        if (item.data.data.equipped) {
          if (item.data.data.initiativeMod) acc.initiativeMod += item.data.data.initiativeMod;
          if (item.data.type === "armor") acc.armor += item.data.data.rating;
          if (item.data.type === "armor") acc.ballistic += item.data.data.ballistic;
          if (item.data.data.defensePenalty) acc.defense -= item.data.data.defensePenalty;
          if (item.data.data.speedPenalty) acc.speed -= item.data.data.speedPenalty;
        }
        return acc;
      }, {
        initiativeMod: 0,
        defense: 0,
        speed: 0,
        armor: 0,
        ballistic: 0
      });

      let traitBuffs = {
        size: 0,
        speed: 0,
        defense: 0,
        armor: 0,
        initiativeMod: 0,
        ballistic: 0,
        perception: 0
      }

      //Initialise final values to undefined (otherwise they'll not get reset, when they're accidently saved during e.g. migration)
      const attributes = [data.attributes_physical, data.attributes_mental, data.attributes_social, data.skills_physical, data.skills_social, data.skills_mental, data.derivedTraits];
      attributes.forEach(attribute => Object.values(attribute).forEach(trait => {
        trait.final = undefined;
        trait.raw = undefined;
      }));

      const der = data.derivedTraits;

      let derivedTraitBuffs = [];

      //Get effects from items (modifiers to any attribute/skill/etc.)
      for (let i of actorData.items) {
        if (i?.data?.data?.effects && i?.data?.data?.effectsActive) { // only look at active effects
          if(i.type === "form" && actorData.data.characterType !== "Werewolf") continue; // Forms only work for werewolves
          i.data.data.effects.forEach(e => {
            const type = e.name.split('.')[0];
            if(type !== "derivedTraits"){
              const trait = e.name.split('.').reduce((o,i)=> o[i], data);
              trait.raw = Number.isInteger(trait.raw) ?  trait.raw + e.value : trait.value + e.value;
              trait.final = Math.clamped(trait.raw, 0, Math.max(trait.value,CONFIG.MTA.traitMaximum));
            }
            else derivedTraitBuffs.push(e) // Have to be applied at the end
          });
        }
      }
      
      //Determine derived traits
      const str = ActorMtA.getTrait(data.attributes_physical.strength);
      const dex = ActorMtA.getTrait(data.attributes_physical.dexterity);
      const wit = ActorMtA.getTrait(data.attributes_mental.wits);
      const comp = ActorMtA.getTrait(data.attributes_social.composure);

      der.size.value = 5 + der.size.mod;
      der.speed.value = 5 + str + dex + der.speed.mod + item_mods.speed;
      der.defense.value = Math.min(wit, dex) + this._getDefenseSkill() + der.defense.mod + item_mods.defense + traitBuffs.defense;
      if (data.characterType === "Vampire" && (CONFIG.MTA.disciplines_common["celerity"] || CONFIG.MTA.disciplines_unique["celerity"])) der.defense.value += data.disciplines_common.celerity.value;
      der.armor.value = der.armor.mod + item_mods.armor + traitBuffs.armor;
      der.ballistic.value = der.ballistic.mod + item_mods.ballistic + traitBuffs.ballistic;
      der.initiativeMod.value = comp + dex + der.initiativeMod.mod + item_mods.initiativeMod + traitBuffs.initiativeMod;
      der.perception.value = comp + wit + der.perception.mod + traitBuffs.perception;

      // Apply derived Traits buffs
      derivedTraitBuffs.forEach(e => {
        const trait = e.name.split('.').reduce((o,i)=> o[i], data);
        trait.raw = Number.isInteger(trait.raw) ?  trait.raw + e.value : trait.value + e.value;
        trait.final = Math.max(trait.raw, 0);
      });

      der.size.value = Math.max(0, der.size.value);
      der.speed.value = Math.max(0, der.speed.value);
      der.defense.value = Math.max(0, der.defense.value);
      der.armor.value = Math.max(0, der.armor.value);
      der.ballistic.value = Math.max(0, der.ballistic.value);
      der.initiativeMod.value = Math.max(0, der.initiativeMod.value);
      der.perception.value = Math.max(0, der.perception.value);

      der.initiativeMod.computed = ActorMtA.getTrait(der.initiativeMod); // Lazy fix for initiative formula

    } else if (actorData.type === "ephemeral") {
      const der = data.derivedTraits;

      //Determine derived traits
      der.size.final = 5 + der.size.mod;
      der.speed.final = 5 + data.eph_physical.power.value + data.eph_social.finesse.value + der.speed.mod;
      der.defense.final = (data.rank > 1 ? Math.min(data.eph_physical.power.value, data.eph_social.finesse.value) : Math.max(data.eph_physical.power.value, data.eph_social.finesse.value)) + der.defense.mod;
      der.armor.final = der.armor.mod;
      der.ballistic.final = der.ballistic.mod;
      der.initiativeMod.final = data.eph_social.finesse.value + data.eph_mental.resistance.value + der.initiativeMod.mod;
    }
  }

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers
  /* -------------------------------------------- */

  /** @override */
  static async create(data, options = {}) {
    data.token = data.token || {};
    if (data.type === "character") {
      mergeObject(data.token, {
        vision: false,
        dimSight: 30,
        brightSight: 0,
        actorLink: false,
        disposition: 0,
        displayBars: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,
        bar1: {
          attribute: "health"
        },
        displayName: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER
      }, {
        overwrite: false
      });
    }

    const actor = await super.create(data, options);
    if(actor.data.type === "character") await actor.createWerewolfForms();
    
    return actor;
  }

  /**
   * Creates the 5 standard werewolf forms for the actor, and
   * deletes all existing forms.
   */
  async createWerewolfForms(){
    //Add the 5 basic werewolf forms
    const itemData = [
      {
        name: "Hishu",
        type: "form",
        img: "systems/mta/icons/forms/Hishu.svg",
        data: {
          subname: "Human",
          effects: [
            {name: "derivedTraits.perception", value: 1}
          ],
          description_short: "Sheep's Clothing",
          description: "",
          effectsActive: true
        }
      },
      {
        name: "Dalu",
        type: "form",
        img: "systems/mta/icons/forms/Dalu.svg",
        data: {
          subname: "Near-Human",
          effects: [
            {name: "attributes_physical.strength", value: 1},
            {name: "attributes_physical.stamina", value: 1},
            {name: "attributes_social.manipulation", value: -1},
            {name: "derivedTraits.size", value: 1},
            {name: "derivedTraits.perception", value: 2}
          ],
          description_short: "Teeth/Claws +0L\nDefense vs. Firearms\nMild Lunacy\nBadass Motherfucker",
          description: ""
        }
      },
      {
        name: "Gauru",
        type: "form",
        img: "systems/mta/icons/forms/Gauru.svg",
        data: {
          subname: "Wolf-Man",
          effects: [
            {name: "attributes_physical.strength", value: 3},
            {name: "attributes_physical.dexterity", value: 1},
            {name: "attributes_physical.stamina", value: 2},
            {name: "derivedTraits.size", value: 2},
            {name: "derivedTraits.perception", value: 3}
          ],
          description_short: "Teeth/Claws +2L\n(Initiative +3)\nDefense vs. Firearms\nFull Lunacy\nRegeneration\nRage\nPrimal Fear",
          description: ""
        }
      },
      {
        name: "Urshul",
        type: "form",
        img: "systems/mta/icons/forms/Urshul.svg",
        data: {
          subname: "Near-Wolf",
          effects: [
            {name: "attributes_physical.strength", value: 2},
            {name: "attributes_physical.dexterity", value: 2},
            {name: "attributes_physical.stamina", value: 2},
            {name: "attributes_social.manipulation", value: -1},
            {name: "derivedTraits.size", value: 1},
            {name: "derivedTraits.speed", value: 3},
            {name: "derivedTraits.perception", value: 3}
          ],
          description_short: "Teeth +2L/Claws +1L\nDefense vs. Firearms\nModerate Lunacy\nWeaken the Prey",
          description: ""
        }
      },
      {
        name: "Urhan",
        type: "form",
        img: "systems/mta/icons/forms/Urhan.svg",
        data: {
          subname: "Wolf",
          effects: [
            {name: "attributes_physical.dexterity", value: 2},
            {name: "attributes_physical.stamina", value: 1},
            {name: "attributes_social.manipulation", value: -1},
            {name: "derivedTraits.size", value: -1},
            {name: "derivedTraits.speed", value: 3},
            {name: "derivedTraits.perception", value: 4}
          ],
          description_short: "Teeth +1L\nChase Down",
          description: ""
        }
      }
    ];
    let oldForms = this.data.items.filter(item => item.type === "form").map(item => item.id);
    if(oldForms) await this.deleteEmbeddedDocuments("Item",oldForms);
    await this.createEmbeddedDocuments("Item", itemData);
  }

  /**
   * This function returns the actual, modified value for any attribute or skill.
   * and should be used whenever someone rolls.
   * Final is the modified trait, and value is the base.
   * Do not use this for derived traits! They do not have value or final fields.
   */
  static getTrait(trait){
    if(!trait) {
      ui.notifications.error("The requested trait does not exist (" + trait + ")");
      return 0;
    }
    return Number.isInteger(trait.final) ? trait.final : trait.value;
  }

  //Search for Merit Defensive Combat
  _getDefenseSkill() {
    const actorData = this.data;
    const data = actorData.data;

    const hasBrawlMerit = this.data.items.find(ele => {
      return ele.name === "Defensive Combat (Brawl)" && ele.type === "merit";
    });
    let hasWeaponryMerit = this.data.items.find(ele => {
      return ele.name === "Defensive Combat (Weaponry)" && ele.type === "merit";
    });
    if (hasWeaponryMerit) {
      hasWeaponryMerit = this.data.items.find(ele => {
        return ele.data.equipped && ele.type === "melee";
      });
    }

    const brawlSkill = hasBrawlMerit ? ActorMtA.getTrait(data.skills_physical.brawl)  : 0;
    const weaponrySkill = hasWeaponryMerit ? ActorMtA.getTrait(data.skills_physical.weaponry): 0;
    return Math.max(Math.max(brawlSkill, weaponrySkill), ActorMtA.getTrait(data.skills_physical.athletics));
  }

  /**
   * Executes a perception roll using Composure + Wits.
   * if hidden is set, the roll is executed as a blind GM roll.
   */
  rollPerception(quickRoll, hidden, actorOverride) {
    const data = this.data.data;
    let dicepool = ActorMtA.getTrait(data.derivedTraits.perception);
    let flavor = "Perception: Composure + Wits";
    if (quickRoll) DiceRollerDialogue.rollToChat({
      dicePool: dicepool,
      flavor: flavor,
      blindGMRoll: hidden,
      actorOverride: actorOverride
    });
    else {
      let diceRoller = new DiceRollerDialogue({
        dicePool: dicepool,
        flavor: flavor,
        addBonusFlavor: true,
        blindGMRoll: true,
        actorOverride: actorOverride
      });
      diceRoller.render(true);
    }
  }

  castSpell(spell){
    const itemData = spell ? duplicate(spell.data.data) : {};
    if (spell) {
      if (spell.data.data.isRote) itemData.castRote = true;
      else if (spell.data.data.isPraxis) itemData.castPraxis = true;
    }

    let activeSpell = new CONFIG.Item.documentClass({
      data: mergeObject(game.system.model.Item["activeSpell"], itemData, {
        inplace: false
      }),
      name: spell ? spell.name : 'Improvised Spell',
      img: spell ? spell.img : '',
      type:  "activeSpell"
    }); 

    activeSpell.data.img = spell ? spell.img : '';
    
    let spellDialogue = new ImprovisedSpellDialogue(activeSpell, this);
    spellDialogue.render(true);
  }

  /**
   * Executes a breaking point roll using Resolve + Composure.
   * if hidden is set, the roll is executed as a blind GM roll.
   */
  rollBreakingPoint(quickRoll, hidden) {
    const data = this.data.data;
    let dicepool = ActorMtA.getTrait(data.attributes_social.composure) + ActorMtA.getTrait(data.attributes_mental.resolve);
    let penalty = data.integrity >= 8 ? 2 : data.integrity >= 6 ? 1 : data.integrity <= 1 ? -2 : data.integrity <= 3 ? -1 : 0;
    dicepool += penalty;
    let flavor = "Breaking Point: Resolve + Composure + " + penalty;
    if (quickRoll) DiceRollerDialogue.rollToChat({
      dicePool: dicepool,
      flavor: flavor,
      blindGMRoll: hidden
    });
    else {
      let diceRoller = new DiceRollerDialogue({
        dicePool: dicepool,
        flavor: flavor,
        addBonusFlavor: true,
        blindGMRoll: hidden
      });
      diceRoller.render(true);
    }
  }

  /**
   * Executes a dissonance roll using Integrity.
   * if hidden is set, the roll is executed as a blind GM roll.
   */
  rollDissonance(quickRoll, hidden) {
    const data = this.data.data;
    let dicepool = data.integrity;
    let flavor = "Dissonance: Integrity (withstood by spell dots, rank, soul stones, etc.)";
    if (quickRoll) DiceRollerDialogue.rollToChat({
      dicePool: dicepool,
      flavor: flavor,
      blindGMRoll: hidden
    });
    else {
      let diceRoller = new DiceRollerDialogue({
        dicePool: dicepool,
        flavor: flavor,
        addBonusFlavor: true,
        blindGMRoll: hidden
      });
      diceRoller.render(true);
    }
  }

  /**
   * Prompts the user with a dialogue to enter name and beats to add
   * a progress entry to the actor.
   */
  addProgressDialogue() {
    let d = new Dialog({
      title: "Add Progress",
      content: "<div> <span> Name </span> <input class='attribute-value' type='text' name='input.name' placeholder='No Reason'/></div> <div> <span> Beats </span> <input class='attribute-value' type='number' name='input.beats' data-dtype='Number' min='0' placeholder='0'/></div> <div> <span> Arc. Beats </span> <input class='attribute-value' type='number' name='input.arcaneBeats' data-dtype='Number' min='0' placeholder='0'/></div>",
      buttons: {
        ok: {
          icon: '<i class="fas fa-check"></i>',
          label: "OK",
          callback: html => {
            let name = html.find(".attribute-value[name='input.name']").val();
            if (!name) name = "No Reason";
            let beats = html.find(".attribute-value[name='input.beats']").val();
            if (!beats) beats = 0;
            let arcaneBeats = html.find(".attribute-value[name='input.arcaneBeats']").val();
            if (!arcaneBeats) arcaneBeats = 0;
            this.addProgress(name, beats, arcaneBeats);
          }
        },
        cancel: {
          icon: '<i class="fas fa-times"></i>',
          label: "Cancel"
        }
      },
      default: "cancel"
    });
    d.render(true);
  }

  /**
   * Adds a progress entry to the actor, with given name and beats.
   */
  addProgress(name = "", beats = 0, arcaneBeats = 0) {
    const data = this.data.data;
    beats = Math.floor(beats);
    arcaneBeats = Math.floor(arcaneBeats);
    let progress = data.progress ? duplicate(data.progress) : [];
    progress.push({
      name: name,
      beats: beats,
      arcaneBeats: arcaneBeats
    });
    return this.update({
      'data.progress': progress
    });
  }

  /**
   * Removes a progress entry from the actor at a given position.
   * Note, that the first entry (__INITIAL__) is not part of the progress array;
   * the element coming after it has index 0.
   */
  removeProgress(index = 0) {
    const data = this.data.data;
    let progress = data.progress ? duplicate(data.progress) : [];
    progress.splice(index, 1);
    return this.update({
      'data.progress': progress
    });
  }

  /**
   * Calculates and sets the maximum health for the actor using the formula
   * Stamina + Size.
   * If health is set lower than any damage, the damage is lost.
   */
  calculateAndSetMaxHealth() {
    const data = this.data.data;
    const maxHealth_old = data.health.max;
    let maxHealth = ActorMtA.getTrait(data.derivedTraits.size) + ActorMtA.getTrait(data.attributes_physical.stamina);
    //if(data.characterType === "Vampire") maxHealth += data.disciplines.common.resilience.value;

    let obj = {}
    obj['data.health.max'] = maxHealth;

    let diff = maxHealth - maxHealth_old;
    if (diff > 0) {
      obj['data.health.lethal'] = "" + (+data.health.lethal + diff);
      obj['data.health.aggravated'] = "" + (+data.health.aggravated + diff);
      obj['data.health.value'] = "" + (+data.health.value + diff);
    } else {
      obj['data.health.lethal'] = "" + Math.max(0, (+data.health.lethal + diff));
      obj['data.health.aggravated'] = "" + Math.max(0, (+data.health.aggravated + diff));
      obj['data.health.value'] = "" + Math.max(0, (+data.health.value + diff));
    }
    this.update(obj);
  }

  /**
   * Calculates and sets the maximum splat-specific ressource for the actor.
   * Mage: Mana (determined by Gnosis)
   * Vampire: Vitae (determined by Blood Potency)
   */
  calculateAndSetMaxResource() {
    const data = this.data.data;
    if (data.characterType === "Mage" || data.characterType === "Proximi" || data.characterType === "Scelesti") { // Mana
      let maxResource = (data.characterType === "Mage" || data.characterType === "Scelesti") ? CONFIG.MTA.gnosis_levels[Math.min(9, Math.max(0, data.mage_traits.gnosis - 1))].max_mana : 5;
      let obj = {}
      obj['data.mana.max'] = maxResource;
      this.update(obj);
    } else if (data.characterType === "Vampire") { // Vitae
      let maxResource = CONFIG.MTA.bloodPotency_levels[Math.min(10, Math.max(0, data.vampire_traits.bloodPotency))].max_vitae;
      if (data.vampire_traits.bloodPotency < 1) maxResource = ActorMtA.getTrait(data.attributes_physical.stamina)

      let obj = {}
      obj['data.vitae.max'] = maxResource;
      this.update(obj);
    } else if (data.characterType === "Werewolf") { // Vitae
      let maxResource = CONFIG.MTA.primalUrge_levels[Math.min(9, Math.max(0, data.werewolf_traits.primalUrge - 1))].max_essence;

      let obj = {}
      obj['data.essence.max'] = maxResource;
      this.update(obj);
    }
  }

  /**
   * Calculates and sets the maximum clarity for the actor using the formula
   * Wits + Composure.
   * If clarity is set lower than any damage, the damage is lost.
   * Also calls updateChangelingTouchstones().
   */
  async calculateAndSetMaxClarity() {
    const data = this.data.data;
    const maxClarity_old = data.clarity.max;
    let maxClarity = ActorMtA.getTrait(data.attributes_mental.wits) + ActorMtA.getTrait(data.attributes_social.composure);

    let obj = {}
    obj['data.clarity.max'] = maxClarity;

    let diff = maxClarity - maxClarity_old;
    if (diff > 0) {
      obj['data.clarity.severe'] = "" + (+data.clarity.severe + diff);
      obj['data.clarity.value'] = "" + (+data.clarity.value + diff);
    } else {
      obj['data.clarity.severe'] = "" + Math.max(0, (+data.clarity.severe + diff));
      obj['data.clarity.value'] = "" + Math.max(0, (+data.clarity.value + diff));
    }
    await this.update(obj);
    this.updateChangelingTouchstones();
  }

  /**
   * Updates the number of touchstones based on the maximum clarity.
   */
  updateChangelingTouchstones() {
    const data = this.data.data;
    let touchstones = duplicate(data.touchstones_changeling);
    let touchstone_amount = Object.keys(touchstones).length;
    if (touchstone_amount < data.clarity.max) {
      while (touchstone_amount < data.clarity.max) {
        touchstones[touchstone_amount + 1] = "";
        touchstone_amount = Object.keys(touchstones).length;
      }
    } else if (touchstone_amount > data.clarity.max) {
      while (touchstone_amount > data.clarity.max) {
        touchstones['-=' + touchstone_amount] = null;
        touchstone_amount -= 1;
      }
    }
    let obj = {};
    obj['data.touchstones_changeling'] = touchstones;
    this.update(obj);
  }
}