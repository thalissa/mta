export const registerSystemSettings = function() {

  /**
   * Track the system version upon which point a migration was last applied
   */
  game.settings.register("mta", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: false,
    type: String,
    default: ""
  });
  
  /**
   * Option to automatically collapse Item Card descriptions
   */
  game.settings.register("mta", "autoCollapseItemDescription", {
    name: "Collapse Item Cards in Chat",
    hint: "Automatically collapse Item Card descriptions in the Chat Log",
    scope: "client",
    config: true,
    default: true,
    type: Boolean,
    onChange: s => {
      ui.chat.render();
    }
  });


  /**
   * Option to automatically collapse Item Card descriptions
   */
   game.settings.register("mta", "showRollDifficulty", {
    name: "Show roll difficulty setting",
    hint: "Add option to change roll difficulty in the dice roller",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: s => {
      
    }
  });

  game.settings.register("mta", "showConditionsOnTokens", {
    name: "Show Conditions icons on Tokens",
    hint: "Determines whether Conditions are shown as status icons on Tokens for the user",
    scope: "client",
    config: true,
    default: true,
    type: Boolean,
    onChange: s => {
      
    }
  });
};
